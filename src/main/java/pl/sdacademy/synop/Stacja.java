package pl.sdacademy.synop;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * {
 *       "id_stacji":"12295",
 *       "stacja":"Bia\u0142ystok",
 *       "data_pomiaru":"2021-04-27",
 *       "godzina_pomiaru":"15",
 *       "temperatura":"7.3",
 *       "predkosc_wiatru":"3",
 *       "kierunek_wiatru":"300",
 *       "wilgotnosc_wzgledna":"53.7",
 *       "suma_opadu":"0.5",
 *       "cisnienie":"1016.1"
 *    }
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Stacja {
    @JsonProperty("id_stacji")
    private Integer idStacji;
    private String stacja;
    private Double temperatura;
    @JsonProperty("data_pomiaru")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dataPomiaru;
    private Double cisnienie;
    @JsonProperty("wilgotnosc_wzgledna")
    private Double wilgotosc;
    @JsonProperty("suma_opadu")
    private Double sumaOpadu;
    @JsonProperty("predkosc_wiatru")
    private Integer predkoscWiatru;
    @JsonProperty("kierunek_wiatru")
    private Integer kierunekWiatru;


    public Double getCisnienie() {
        return cisnienie;
    }

    public void setCisnienie(Double cisnienie) {
        this.cisnienie = cisnienie;
    }

    public Double getWilgotosc() {
        return wilgotosc;
    }

    public void setWilgotosc(Double wilgotosc) {
        this.wilgotosc = wilgotosc;
    }

    public Double getSumaOpadu() {
        return sumaOpadu;
    }

    public void setSumaOpadu(Double sumaOpadu) {
        this.sumaOpadu = sumaOpadu;
    }

    public Integer getPredkoscWiatru() {
        return predkoscWiatru;
    }

    public void setPredkoscWiatru(Integer predkoscWiatru) {
        this.predkoscWiatru = predkoscWiatru;
    }

    public Integer getKierunekWiatru() {
        return kierunekWiatru;
    }

    public void setKierunekWiatru(Integer kierunekWiatru) {
        this.kierunekWiatru = kierunekWiatru;
    }

    public Date getDataPomiaru() {
        return dataPomiaru;
    }

    public void setDataPomiaru(Date dataPomiaru) {
        this.dataPomiaru = dataPomiaru;
    }

    public Integer getIdStacji() {
        return idStacji;
    }

    public void setIdStacji(Integer idStacji) {
        this.idStacji = idStacji;
    }

    public String getStacja() {
        return stacja;
    }

    public void setStacja(String stacja) {
        this.stacja = stacja;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }
}
