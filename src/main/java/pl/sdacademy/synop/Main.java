package pl.sdacademy.synop;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) throws IOException {
        String url = "https://danepubliczne.imgw.pl/api/data/synop";
        ObjectMapper om = new ObjectMapper();

        Stacja[] stacje = om.readValue(new URL(url), Stacja[].class);

        Stacja maxTemp = Arrays.asList(stacje)
                .stream()
                .max(Comparator.comparing(Stacja::getTemperatura))
                .get();

        Stacja minTemp = Arrays.asList(stacje)
                .stream()
                .min(Comparator.comparing(Stacja::getTemperatura))
                .get();

        Stacja maxCisnienie = Arrays.asList(stacje)
                .stream()
                .filter(stacja -> stacja.getCisnienie() != null)
                .max(Comparator.comparing(Stacja::getCisnienie))
                .get();

        Stacja minCisnienie = Arrays.asList(stacje)
                .stream()
                .filter(stacja -> stacja.getCisnienie() != null)
                .min(Comparator.comparing(Stacja::getCisnienie))
                .get();

        Stacja maxOpad = Arrays.asList(stacje)
                .stream()
                .filter(stacja -> stacja.getSumaOpadu() != null)
                .max(Comparator.comparing(Stacja::getSumaOpadu))
                .get();

        Stacja minOpad = Arrays.asList(stacje)
                .stream()
                .filter(stacja -> stacja.getSumaOpadu() != null)
                .min(Comparator.comparing(Stacja::getSumaOpadu))
                .get();

        long countCisnienie = Arrays.asList(stacje)
                .stream()
                .filter(stacja -> stacja.getCisnienie() != null)
                .count();

        double sumTemp = Arrays.asList(stacje)
                .stream()
                .map(stacja -> stacja.getTemperatura())
                .reduce(0.0, (acc, temperatura) -> acc + temperatura);

        double sumWet = Arrays.asList(stacje)
                .stream()
                .map(stacja -> stacja.getWilgotosc())
                .reduce(0.0, (sumWW, wilgotnoscWzgledna) -> sumWW + wilgotnoscWzgledna);

        double sumCisnienie = Arrays.stream(stacje)
                .filter(station -> station.getCisnienie() != null)
                .map(Stacja::getCisnienie)
                .reduce(Double::sum)
                .get();

        double sumOpady = Arrays.stream(stacje)
                .filter(station -> station.getSumaOpadu() != null)
                .map(Stacja::getSumaOpadu)
                .reduce(Double::sum)
                .get();


        System.out.println("Najcieplej jest w " + maxTemp.getStacja() + " " + maxTemp.getTemperatura());
        System.out.println("Najchlodniej jest w " + minTemp.getStacja() + " " + minTemp.getTemperatura());

        System.out.println("Max cisnienie w " + maxCisnienie.getStacja() + " " + maxCisnienie.getCisnienie());
        System.out.println("Min cisnienie w " + minCisnienie.getStacja() + " " + minCisnienie.getCisnienie());

        System.out.println("Max suma opadu w " + maxOpad.getStacja() + " " + maxOpad.getSumaOpadu());
        System.out.println("Min suma opadu w " + minOpad.getStacja() + " " + minOpad.getSumaOpadu());

        System.out.println("Srednia temparatura: " + (sumTemp / stacje.length));
        System.out.println("Srednia wilgotnosc: " + (sumWet / stacje.length));
        System.out.println("Srednia cisnienie: " + (sumCisnienie / countCisnienie));
        System.out.println("Srednia suma opadow: " + (sumOpady / stacje.length));
    }
}
